# pybf-interpreter

Just a very simple brainf--- interpreter written in Python. 
To use it just navigate to the root folder and use:
    python3 bf_interpreter.py examples/hello_world.bf
There are also a couple of more examples in the examples folder.