from collections import defaultdict
import argparse
from pathlib import Path


def find_matching_closing_bracket_position(code: str, code_pointer: int) -> int:
    encountered_open = 1
    encountered_close = 0
    while encountered_open != encountered_close:
        code_pointer += 1
        if code[code_pointer] == "[":
            encountered_open += 1
        elif code[code_pointer] == "]":
            encountered_close += 1
    return code_pointer


def find_matching_opening_bracket_position(code: str, code_pointer: int) -> int:
    encountered_open = 0
    encountered_close = 1
    while encountered_open != encountered_close:
        code_pointer -= 1
        if code[code_pointer] == "[":
            encountered_open += 1
        elif code[code_pointer] == "]":
            encountered_close += 1
    return code_pointer


def evaluate_bf(code: str):
    pointer_position = 0
    infinite_array = defaultdict(lambda: 0)
    code_pointer = 0
    length_code = len(code)
    iterations = 0

    while code_pointer < length_code:
        #debug_array = [value for value in infinite_array.values()]
        #print(
        #   f"IT{iterations}:\n"
        #   f"\tcode pointer: {code_pointer}/{length_code} with value {code[code_pointer]}\n"
        #   f"\tarray_state: {debug_array}\n"
        #   f"\tcurrent_pointer_value: {pointer_position}->{infinite_array[pointer_position]}\n"
        #)
        char = code[code_pointer]
        if char == "+":
            infinite_array[pointer_position] += 1
            if infinite_array[pointer_position] == 256:
                infinite_array[pointer_position] = 0
        elif char == "-":
            infinite_array[pointer_position] -= 1
            if infinite_array[pointer_position] == -1:
                infinite_array[pointer_position] = 255
        elif char == ".":
            print(chr(infinite_array[pointer_position]), end="")
        elif char == ">":
            pointer_position += 1
        elif char == "<":
            pointer_position -= 1
        elif char == "[":
            if infinite_array[pointer_position] == 0:
                code_pointer = find_matching_closing_bracket_position(
                    code, code_pointer
                )
            else:
                pass
        elif char == "]":
            code_pointer = (
                find_matching_opening_bracket_position(code, code_pointer) - 1
            )

        code_pointer += 1
        iterations += 1

    print(f"\nFinished after {iterations} iterations")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filepath")
    args = parser.parse_args()
    try:
        path = Path(args.filepath)
    except Exception:
        raise Exception("Error: invalid path")
    with open(path, "r") as file:
        code = file.read()
        evaluate_bf(code)


